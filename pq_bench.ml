open Core
open Core_bench

let compare n m = if n = m then 0 else if n < m then -1 else 1

module B = Pq.Heap.Make (struct
  type t = int

  let compare = compare
end)

(* let test_get_functions ?(l=l100) () =
 *   let len = List.length l in 
 *   let h = B.create len in
 *   Stdlib.List.iter (fun e -> B.insert e h) l;
 *   let h' = B.{ h with data = Array.copy h.data } in
 *   Core.Command.run
 *     (Bench.make_command
 *        [
 *          Bench.Test.create ~name:"get (pop)" (fun () -> B.get_base h);
 *          Bench.Test.create ~name:"get (pop/no init)" (fun () -> B.get_base2 h);
 *          Bench.Test.create ~name:"get (no Array.copy)" (fun () ->
 *              B.get_no_copy h');
 *          Bench.Test.create ~name:"get (List.sort)" (fun () -> B.get_list h);
 *          Bench.Test.create ~name:"get_array (Array.sort)" (fun () -> B.get_array h;);
 *          Bench.Test.create ~name:"get (Array.sort)" (fun () -> B.get_list_of_array h);
 *        ]) *)

let test_take_sorted () =
  let open Pq in
  let tests = [Common.range 1 10; Common.l100; Common.l10000; Common.range 1 100000] in
  Stdlib.List.iter (fun l ->
      let len = List.length l in
      (* Fix a size where reorganizations are bound to happen when inserting
         bigger objects - we are manipulating a min-heap. *)
      let n = len / 2 in
      Format.printf "## List size: %d - Heap size: %n@." len n;
      Core.Command.run
        Bench.(
        make_command
          [
            Test.create ~name:"Bounded" (fun () ->
                Simple.take_n_sorted Stdlib.compare n l);
            Test.create ~name:"Make" (fun () ->
                Heap.take_n_sorted Stdlib.compare n l);
            Test.create ~name:"No functor" (fun () -> Pq.Heap_no_functor.take_n_sorted Stdlib.compare n l)
    ])) tests
;;

(* test_get_functions () ;;
 * 
 * test_get_functions ~l:(Common.range 1 10000) () ;;
 * 
 * test_get_functions ~l:(Common.range 1 1000000) () ;; *)

test_take_sorted () ;;
