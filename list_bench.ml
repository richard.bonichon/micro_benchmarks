open Core
open Core_bench
open Common

let concat l1 l2 = l1 @ l2

let fold_conc l1 l2 = List.fold_left ~f:(fun l e -> e :: l) ~init:l1 l2

let fold_conc2 l1 l2 =
  List.fold_left ~f:(fun l e -> e :: l) ~init:(List.rev l1) l2

let add_right l = List.fold_right ~f:( + ) ~init:0 l

let add_left l = List.fold_left ~f:( + ) ~init:0 (List.rev l)

let test () =
  (* Core.Command.run
   *   (Bench.make_command [
   *       Bench.Test.create ~name:"append"
   *         (fun () -> concat l l);
   *       Bench.Test.create ~name:"fold append"
   *         (fun () -> fold_conc l l);
   *       Bench.Test.create ~name:"fold append rev"
   *         (fun () -> fold_conc2 l l);
   *   ]); *)
  Core.Command.run
    (Bench.make_command
       [
         Bench.Test.create ~name:"fold right" (fun () -> add_right l100);
         Bench.Test.create ~name:"fold left" (fun () -> add_left l100);
       ])

;;
test ()
