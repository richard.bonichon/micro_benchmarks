module Simple = struct
  module Make (E : Stdlib.Set.OrderedType) = struct
    type t = { bound : int; mutable size : int; mutable data : E.t list }

    let create bound =
      if bound <= 0 then invalid_arg "Utils.Bounded(_).create";
      { bound; size = 0; data = [] }

    let rec push x = function
      | [] -> [ x ]
      | y :: xs as ys -> if E.compare x y <= 0 then x :: ys else y :: push x xs

    let insert x t =
      if t.size < t.bound then (
        t.size <- t.size + 1;
        t.data <- push x t.data)
      else
        match t.data with
        | [] -> assert false
        | hd :: tl -> if E.compare hd x < 0 then t.data <- push x tl

    let get { data; _ } = data
  end

  let take_n_sorted (type a) compare n l =
    let module B = Make (struct
      type t = a

      let compare = compare
    end) in
    let t = B.create n in
    Stdlib.List.iter (fun x -> B.insert x t) l;
    B.get t
end

module Heap_no_functor = struct
  module H = struct
  open Stdlib
    (* Implementation of a min-heap for bounded multisets of values *)

    (* Invariants :
       - all indices between 0 and size (excluded) have data:
         size =
           Array.fold_left
           (fun count e -> if Option.is_some e then count + 1 else count) 0 data
       - the heap property itself:
          let P(i, j) =
            0 <= i < size /\ 0 <= j < size =>
            data.(j) = None \/ E.compare data.(i) data.(j) <= 0
         in \forall 0 <= i < size, P(i, 2 * i + 1) /\ P(i, 2 * (i + 1))
    *)
    type 'a t = { mutable size : int; mutable data : 'a option array }

    let create bound =
      if bound < 0 || bound > Sys.max_array_length then
        invalid_arg "TzList.Bounded(_).create";
      { size = 0; data = Array.make bound None }

    let peek { size; data } = if size = 0 then None else data.(0)

    let get_data data i = Option.get data.(i)

    let swap i j data =
      let tmp = data.(j) in
      data.(j) <- data.(i);
      data.(i) <- tmp

    (* Bubble up the value located at index [i] such until the heap property is
       locally fulfilled *)
    let bubble_up ~compare i { data; size = _ } =
      let rec loop i =
        if i > 0 then
          let j = (i - 1) / 2 in
          if compare (get_data data j) (get_data data i) > 0 then (
            swap i j data;
            loop j)
      in
      loop i

    (* Bubble down the value at index [i] until the heap property is locally
       fulfilled, aka the value at index [i] is smaller than the one of its two
       children *)
    let bubble_down ~compare i { size; data } =
      let rec loop i =
        let left_index = (2 * i) + 1 in
        if left_index < size then (
          assert (data.(left_index) <> None);
          let left_value = get_data data left_index in
          let v = get_data data i in
          let right_index = 2 * (i + 1) in
          if right_index < size then (
            (* swap the value with the smallest of its two children *)
            assert (data.(right_index) <> None);
            let right_value = get_data data right_index in
            if compare right_value left_value < 0 then (
              if compare v right_value > 0 then (
                swap i right_index data;
                loop right_index))
            else if compare v left_value > 0 then (
              swap i left_index data;
              loop left_index))
          else if
            compare v left_value > 0
            (* swap the value with its left child, since the right one does not exist *)
          then (
            swap i left_index data;
            loop left_index))
      in
      loop i

    let insert (compare: 'a -> 'a -> int) (x:'a) (t:'a t): unit  =
      let pos = t.size in
      let data = t.data in
      if pos < Array.length data then (
        data.(pos) <- Some x;
        t.size <- pos + 1;
        bubble_up ~compare pos t)
      else
        match peek t with
        | None ->
            assert (t.size = 0);
            (* This should only happen in the case of the empty structure *)
            ()
        | Some hd ->
            (* if the inserted element is greater than the one at the top of the heap,
               put it it in its place and bubble it down where it belongs *)
            if compare x hd > 0 then (
              data.(0) <- Some x;
              bubble_down ~compare 0 t)

    (* [pop] is an internal function used only in [get] below.
       Be careful, it (of course) mutates the data array. *)
    let pop ~compare ({ data; size } as t) =
      if size > 0 then (
        let last = size - 1 in
        let v = get_data data 0 in
        data.(0) <- data.(last);
        (* data.(last) <- None ; *\) *)
        t.size <- last;
        bubble_down ~compare 0 t;
        v)
      else invalid_arg "TzList.Bounded(...).pop: empty data"

    let get_array ~compare t =
      let a = Array.init t.size (fun i -> get_data t.data i) in
      Array.sort compare a;
      a

    let get ~compare t = Array.to_list (get_array ~compare t)

  end

  let take_n_sorted compare n l =
    let t = H.create n in
    Stdlib.List.iter (fun x -> H.insert compare x t) l;
    H.get ~compare t
end



module Heap = struct
  module Make (E : Stdlib.Set.OrderedType) = struct
    open Stdlib
    (* Implementation of a min-heap for bounded multisets of values *)

    (* Invariants :
       - all indices between 0 and size (excluded) have data:
         size =
           Array.fold_left
           (fun count e -> if Option.is_some e then count + 1 else count) 0 data
       - the heap property itself:
          let P(i, j) =
            0 <= i < size /\ 0 <= j < size =>
            data.(j) = None \/ E.compare data.(i) data.(j) <= 0
         in \forall 0 <= i < size, P(i, 2 * i + 1) /\ P(i, 2 * (i + 1))
    *)
    type t = { mutable size : int; mutable data : E.t option array }

    let create bound =
      if bound < 0 || bound > Sys.max_array_length then
        invalid_arg "TzList.Bounded(_).create";
      { size = 0; data = Array.make bound None }

    let peek { size; data } = if size = 0 then None else data.(0)

    let get_data data i = Option.get data.(i)

    let swap i j data =
      let tmp = data.(j) in
      data.(j) <- data.(i);
      data.(i) <- tmp

    (* Bubble up the value located at index [i] such until the heap property is
       locally fulfilled *)
    let bubble_up i { data; size = _ } =
      let rec loop i =
        if i > 0 then
          let j = (i - 1) / 2 in
          if E.compare (get_data data j) (get_data data i) > 0 then (
            swap i j data;
            loop j)
      in
      loop i

    (* Bubble down the value at index [i] until the heap property is locally
       fulfilled, aka the value at index [i] is smaller than the one of its two
       children *)
    let bubble_down i { size; data } =
      let rec loop i =
        let left_index = (2 * i) + 1 in
        if left_index < size then (
          assert (data.(left_index) <> None);
          let left_value = get_data data left_index in
          let v = get_data data i in
          let right_index = 2 * (i + 1) in
          if right_index < size then (
            (* swap the value with the smallest of its two children *)
            assert (data.(right_index) <> None);
            let right_value = get_data data right_index in
            if E.compare right_value left_value < 0 then (
              if E.compare v right_value > 0 then (
                swap i right_index data;
                loop right_index))
            else if E.compare v left_value > 0 then (
              swap i left_index data;
              loop left_index))
          else if
            E.compare v left_value > 0
            (* swap the value with its left child, since the right one does not exist *)
          then (
            swap i left_index data;
            loop left_index))
      in
      loop i

    let insert x t =
      let pos = t.size in
      let data = t.data in
      if pos < Array.length data then (
        data.(pos) <- Some x;
        t.size <- pos + 1;
        bubble_up pos t)
      else
        match peek t with
        | None ->
            assert (t.size = 0);
            (* This should only happen in the case of the empty structure *)
            ()
        | Some hd ->
            (* if the inserted element is greater than the one at the top of the heap,
               put it it in its place and bubble it down where it belongs *)
            if E.compare x hd > 0 then (
              data.(0) <- Some x;
              bubble_down 0 t)

    (* [pop] is an internal function used only in [get] below.
       Be careful, it (of course) mutates the data array. *)
    let pop ({ data; size } as t) =
      if size > 0 then (
        let last = size - 1 in
        let v = get_data data 0 in
        data.(0) <- data.(last);
        (* data.(last) <- None ; *\) *)
        t.size <- last;
        bubble_down 0 t;
        v)
      else invalid_arg "TzList.Bounded(...).pop: empty data"

    let get_no_copy t =
      let l = ref [] in
      (* Do not destroy existing data *)
      (* let t = {t with data = Array.copy t.data} in *)
      while t.size > 0 do
        l := pop t :: !l
      done;
      List.rev !l

    let get_base t =
      let l = ref [] in
      (* Do not destroy existing data *)
      let data = Array.init t.size (fun i -> t.data.(i)) in
      let t = { t with data } in
      while t.size > 0 do
        l := pop t :: !l
      done;
      List.rev !l

    let get_base2 t =
      let l = ref [] in
      (* Do not destroy existing data *)
      let data = Array.make t.size t.data.(0) in
      for i = 1 to t.size - 1 do
        data.(i) <- t.data.(i)
      done;
      let t = { t with data } in
      while t.size > 0 do
        l := pop t :: !l
      done;
      List.rev !l

    let get_list { size; data } =
      let l = ref [] in
      for i = 0 to size - 1 do
        l := Stdlib.Option.get data.(i) :: !l
      done;
      List.sort E.compare !l

    let get_array t =
      let a = Array.init t.size (fun i -> get_data t.data i) in
      Array.sort E.compare a;
      a

    let get_list_of_array t = Array.to_list (get_array t)

    let get_array2 t =
    match peek t with
    | None ->
        []
    | Some v ->
       let data = Array.init t.size (fun i -> t.data.(i)) in
       let t = { t with data } in
       let a = Array.make t.size v in
       for i = 0 to t.size - 1 do
         a.(i) <- pop t
        done ;
        Array.to_list a

    let get = get_list (* best option *)
  end

  let take_n_sorted (type a) compare n l =
    let module B = Make (struct
      type t = a

      let compare = compare
    end) in
    let t = B.create n in
    Stdlib.List.iter (fun x -> B.insert x t) l;
    B.get t
end
