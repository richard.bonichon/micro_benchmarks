open Core

let range ?f lo hi =
  let f = match f with None -> fun i -> i | Some g -> g in
  Array.init (hi - lo + 1) ~f |> Array.to_list

let l100 = range 1 100

let l10000 = range 1 10000


