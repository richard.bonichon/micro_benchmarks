(* Testing shuffle primitives
Compilation started at Thu Apr 29 20:36:32

dune exec ./shuffle_bench.exe
Benchmark "size 5"
Estimated testing time 50s (5 benchmarks x 10s). Change using '-quota'.
┌─────────────────────────────┬──────────┬─────────┬────────────┐
│ Name                        │ Time/Run │ mWd/Run │ Percentage │
├─────────────────────────────┼──────────┼─────────┼────────────┤
│ shuffle list                │ 152.16ns │ 105.00w │     88.74% │
│ Knuth shuffle               │ 111.08ns │  35.00w │     64.78% │
│ Knuth shuffle_2             │ 141.08ns │  35.00w │     82.28% │
│ inlined Knuth shuffle       │ 148.34ns │  35.00w │     86.51% │
│ inlined Knuth shuffle (for) │ 171.47ns │  31.00w │    100.00% │
└─────────────────────────────┴──────────┴─────────┴────────────┘
Benchmark "size 10"
Estimated testing time 50s (5 benchmarks x 10s). Change using '-quota'.
┌─────────────────────────────┬──────────┬─────────┬────────────┐
│ Name                        │ Time/Run │ mWd/Run │ Percentage │
├─────────────────────────────┼──────────┼─────────┼────────────┤
│ shuffle list                │ 400.92ns │ 222.00w │    100.00% │
│ Knuth shuffle               │ 290.47ns │  55.00w │     72.45% │
│ Knuth shuffle_2             │ 351.73ns │  55.00w │     87.73% │
│ inlined Knuth shuffle       │ 350.56ns │  55.00w │     87.44% │
│ inlined Knuth shuffle (for) │ 301.62ns │  51.00w │     75.23% │
└─────────────────────────────┴──────────┴─────────┴────────────┘
Benchmark "size 100"
Estimated testing time 50s (5 benchmarks x 10s). Change using '-quota'.
┌─────────────────────────────┬──────────┬───────────┬──────────┬──────────┬────────────┐
│ Name                        │ Time/Run │   mWd/Run │ mjWd/Run │ Prom/Run │ Percentage │
├─────────────────────────────┼──────────┼───────────┼──────────┼──────────┼────────────┤
│ shuffle list                │   7.90us │ 2_982.00w │    6.22w │    6.22w │    100.00% │
│ Knuth shuffle               │   3.50us │   415.00w │    0.21w │    0.21w │     44.30% │
│ Knuth shuffle_2             │   3.58us │   415.00w │    0.21w │    0.21w │     45.25% │
│ inlined Knuth shuffle       │   3.55us │   415.00w │    0.22w │    0.22w │     44.92% │
│ inlined Knuth shuffle (for) │   3.32us │   411.00w │    0.28w │    0.28w │     42.03% │
└─────────────────────────────┴──────────┴───────────┴──────────┴──────────┴────────────┘
Benchmark "size 1000"
Estimated testing time 50s (5 benchmarks x 10s). Change using '-quota'.
┌─────────────────────────────┬──────────┬─────────┬───────────┬──────────┬────────────┐
│ Name                        │ Time/Run │ mWd/Run │  mjWd/Run │ Prom/Run │ Percentage │
├─────────────────────────────┼──────────┼─────────┼───────────┼──────────┼────────────┤
│ shuffle list                │ 130.50us │ 41.73kw │   921.63w │  921.63w │    100.00% │
│ Knuth shuffle               │  41.69us │  3.01kw │ 1_019.80w │   18.80w │     31.94% │
│ Knuth shuffle_2             │  37.45us │  3.01kw │ 1_019.75w │   18.75w │     28.70% │
│ inlined Knuth shuffle       │  37.08us │  3.01kw │ 1_019.81w │   18.81w │     28.41% │
│ inlined Knuth shuffle (for) │  34.49us │  3.01kw │ 1_019.80w │   18.80w │     26.43% │
└─────────────────────────────┴──────────┴─────────┴───────────┴──────────┴────────────┘
Benchmark "size 10000"
Estimated testing time 50s (5 benchmarks x 10s). Change using '-quota'.
┌─────────────────────────────┬────────────┬──────────┬──────────┬──────────┬────────────┐
│ Name                        │   Time/Run │  mWd/Run │ mjWd/Run │ Prom/Run │ Percentage │
├─────────────────────────────┼────────────┼──────────┼──────────┼──────────┼────────────┤
│ shuffle list                │ 2_834.17us │ 504.59kw │  81.34kw │  81.34kw │    100.00% │
│ Knuth shuffle               │   410.27us │  30.01kw │  11.87kw │   1.87kw │     14.48% │
│ Knuth shuffle_2             │   409.11us │  30.01kw │  11.87kw │   1.87kw │     14.43% │
│ inlined Knuth shuffle       │   401.22us │  30.01kw │  11.86kw │   1.85kw │     14.16% │
│ inlined Knuth shuffle (for) │   381.96us │  30.01kw │  11.87kw │   1.87kw │     13.48% │
└─────────────────────────────┴────────────┴──────────┴──────────┴──────────┴────────────┘
Benchmark "size 100000"
Estimated testing time 50s (5 benchmarks x 10s). Change using '-quota'.
┌─────────────────────────────┬──────────┬────────────┬────────────┬────────────┬────────────┐
│ Name                        │ Time/Run │    mWd/Run │   mjWd/Run │   Prom/Run │ Percentage │
├─────────────────────────────┼──────────┼────────────┼────────────┼────────────┼────────────┤
│ shuffle list                │  55.65ms │ 5_927.15kw │ 1_907.64kw │ 1_907.64kw │    100.00% │
│ Knuth shuffle               │   6.79ms │   300.01kw │   275.82kw │   175.82kw │     12.20% │
│ Knuth shuffle_2             │   6.41ms │   300.01kw │   270.01kw │   170.01kw │     11.52% │
│ inlined Knuth shuffle       │   5.76ms │   300.01kw │   267.67kw │   167.67kw │     10.35% │
│ inlined Knuth shuffle (for) │   5.47ms │   300.01kw │   267.77kw │   167.77kw │      9.83% │
└─────────────────────────────┴──────────┴────────────┴────────────┴────────────┴────────────┘
 *)

open Core_bench
open Stdlib

let shuffle l =
  l
  |> List.rev_map (fun d -> (Random.bits (), d))
  |> List.sort (fun (x, _) (y, _) -> compare x y)
  |> List.rev_map snd


(* Fisher-Yates shuffle implementation *)
let knuth_shuffle l =
  let knuth_shuffle a =
    let len = Array.length a in
    let swap i j =
      let tmp = a.(i) in
      a.(i) <- a.(j);
      a.(j) <- tmp
    in
    let rec loop n =
      if n > 1 then (
        let m = Random.int n in
        let n' = n - 1 in
        swap m n';
        loop n')
    in
    loop len
  in
  let a = Array.of_list l in
  knuth_shuffle a;
  Array.to_list a

(* Fisher-Yates shuffle implementation 
   call swap only when the indices are different *)
let knuth_shuffle2 l =
  let knuth_shuffle a =
    let len = Array.length a in
    let swap i j =
      let tmp = a.(i) in
      a.(i) <- a.(j);
      a.(j) <- tmp
    in
    let rec loop n =
      if n > 1 then (
        let m = Random.int n in
        let n' = n - 1 in
        if m <> n' then swap m n';
        loop n')
    in
    loop len
  in
  let a = Array.of_list l in
  knuth_shuffle a;
  Array.to_list a


(* Fisher-Yates shuffle implementation 
   do swap only when the indices are different - but always call it *)
let _knuth_shuffle3 l =
  let knuth_shuffle a =
    let len = Array.length a in
    let swap i j =
      if i <> j then 
        let tmp = a.(i) in
        a.(i) <- a.(j);
        a.(j) <- tmp
    in
    let rec loop n =
      if n > 1 then (
        let m = Random.int n in
        let n' = n - 1 in
        swap m n';
        loop n')
    in
    loop len
  in
  let a = Array.of_list l in
  knuth_shuffle a;
  Array.to_list a


(* Fisher-Yates shuffle implementation 
   Every intermediate function (swap) inlined *)
let inlined_shuffle l =
  let a = Array.of_list l in
  let len = Array.length a in
  let rec loop n =
    if n > 1 then (
      let m = Random.int n in
      let n' = n - 1 in
      if m <> n' then
        (let tmp = a.(m) in
         a.(m) <- a.(n');
         a.(n') <- tmp);
      loop n')
    in
    loop len;
  Array.to_list a

(* Fisher-Yates shuffle implementation 
   Every intermediate function (swap) inlined *)
let inlined_shuffle_for l =
  let a = Array.of_list l in
  let len = Array.length a in
  for i = len downto 2 do
    let m = Random.int i in
    let n' = i - 1 in
    if m <> n' then
      (let tmp = a.(m) in
       a.(m) <- a.(n');
       a.(n') <- tmp);
  done ;
  Array.to_list a


let test ~name l =
  Format.printf "Benchmark \"%s\"@." name;
  Core.Command.run
    Bench.(
      make_command
        [
          Test.create ~name:"shuffle list" (fun () -> match shuffle l with exception _ -> () | _ -> ());
          Test.create ~name:"Knuth shuffle" (fun () -> knuth_shuffle l);
          Test.create ~name:"Knuth shuffle_2" (fun () -> knuth_shuffle2 l);
          Test.create ~name:"inlined Knuth shuffle" (fun () -> inlined_shuffle l);
          Test.create ~name:"inlined Knuth shuffle (for)" (fun () -> inlined_shuffle_for l);
        ])

let main () =
  List.iter
    (fun size ->
      let name = Printf.sprintf "size %d" size in
      let list = Common.range 1 size in
      test ~name list)
    [  1000000]

let () = main ()

(* Local Variables: *)
(* compile-command: "dune exec ./shuffle_bench.exe" *)
(* End: *)
