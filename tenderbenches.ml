open Core
open Core_bench

module Compare = struct
  module Int32 = struct
    open Stdlib.Int32

    let ( = ) = Int32.equal

    let ( >= ) a b = compare a b >= 0

    let ( < ) a b = compare a b < 0

    let zero = Int32.zero
  end

  module Int64 = struct
    open Stdlib.Int64

    let ( = ) = Int64.equal

    let zero = Int64.zero

    let ( < ) a b = compare a b < 0

    let ( > ) a b = compare a b > 0

    let ( >= ) a b = compare a b >= 0
  end

  module Z = struct
    let ( > ) a b = Z.compare a b > 0

    let ( < ) a b = Z.compare a b < 0
  end
end

let ( >>? ) v f = Stdlib.Result.map f v

type durations = {
  first_round_duration : int64;
  delay_increment_per_round : int64;
}

let level_offset_of_round (round_durations : durations) ~(round : int32) :
    (int64, string) result =
  if Compare.Int32.(round = zero) then Ok Int64.zero
  else
    let sum_durations =
      let { first_round_duration; delay_increment_per_round } =
        round_durations
      in
      let roundz = Z.of_int32 round in
      let m = Z.(div (mul roundz (pred roundz)) (of_int 2)) in
      Z.(
        add
          (mul m (Z.of_int64 @@ delay_increment_per_round))
          (mul (Z.of_int32 round) (Z.of_int64 @@ first_round_duration)))
    in
    if Compare.Z.(sum_durations > Z.of_int64 Stdlib.Int64.max_int) then
      Error "round_too_hig"
    else Ok (Z.to_int64 sum_durations)

type round_and_offset = { round : int32; offset : int64 }

let pp_roff ppf { round; offset } =
  Format.fprintf ppf "round = %ld; offset = %Ld" round offset

(** Complexity: O(log max_int). *)
let round_and_offset round_durations ~(level_offset : int64) =
  let level_offset_in_seconds = level_offset in
  (* We have the invariant [round <= level_offset] so there is no need to search
     beyond [level_offset]. We set [right_bound] to [level_offset + 1] to avoid
     triggering the error level_offset too high when the round equals
     [level_offset]. *)
  let right_bound =
    if
      Compare.Int64.(
        level_offset_in_seconds < Stdlib.(Int64.of_int32 Int32.max_int))
    then Stdlib.Int32.of_int (Stdlib.Int64.to_int level_offset_in_seconds + 1)
    else Stdlib.Int32.max_int
  in
  let rec bin_search min_r max_r =
    if Compare.Int32.(min_r >= right_bound) then Error "level too high"
    else
      let round = Stdlib.Int32.(add min_r (div (sub max_r min_r) 2l)) in
      let o =
        level_offset_of_round round_durations ~round:(Stdlib.Int32.succ round)
      in
      match o with
      | Error _ as e -> e
      | Ok next_level_offset -> (
          if Compare.Int64.(level_offset >= next_level_offset) then
            bin_search (Int32.succ round) max_r
          else
            match level_offset_of_round round_durations ~round with
            | Error _ as e -> e
            | Ok current_level_offset ->
                if Compare.Int64.(level_offset < current_level_offset) then
                  bin_search min_r round
                else
                  Ok
                    {
                      round;
                      offset =
                        Stdlib.Int64.sub level_offset current_level_offset;
                    })
  in
  bin_search 0l right_bound

(* Using Newton-Raphson for integer square-root. *)
let sqrt_round_and_offset_v1 round_durations ~level_offset =
  let level_offset_in_seconds = level_offset in
  let { first_round_duration; delay_increment_per_round } = round_durations in
  let open Stdlib in
  let round =
    if Compare.Int64.(delay_increment_per_round = Int64.zero) then
      (* Case when delay_increment_per_round is zero and a simple
         linear solution exists. *)
      Int64.div level_offset_in_seconds first_round_duration
    else
      (* Case when the increment is non-negative and we look for the
         quadratic solution. *)
      let pow_2 n = Int64.mul n n in
      let double n = Int64.shift_left n 1 in
      let times_8 n = Int64.shift_left n 3 in
      let half n = Int64.shift_right n 1 in
      (* The integer square root is implemented using the Newton-Raphson
         method. For any integer N, the convergence within the
         neighborhood of √N is ensured within log2 (N) steps. *)
      let sqrt (n : int64) =
        let x0 = ref (half n) in
        if Compare.Int64.(!x0 > 1L) then (
          let x1 = ref (half (Int64.add !x0 (Int64.div n !x0))) in
          while Compare.Int64.(!x1 < !x0) do
            x0 := !x1;
            x1 := half (Int64.add !x0 (Int64.div n !x0))
          done;
          !x0)
        else n
      in

      let discr =
        Int64.add
          (pow_2
             (Int64.sub (double first_round_duration) delay_increment_per_round))
          (times_8
             (Int64.mul delay_increment_per_round level_offset_in_seconds))
      in
      Int64.div
        (Int64.add
           (Int64.sub delay_increment_per_round (double first_round_duration))
           (sqrt discr))
        (double delay_increment_per_round)
  in
  match level_offset_of_round round_durations ~round:(Int64.to_int32 round) with
  | Ok current_level_offset ->
      Ok
        {
          round = Int64.to_int32 round;
          offset = Int64.sub level_offset current_level_offset;
        }
  | Error _ as e -> e

(* Using 4-adic induction for integer square-root. *)
let sqrt_round_and_offset_v2 round_durations ~level_offset =
  let level_offset_in_seconds = level_offset in
  let { first_round_duration; delay_increment_per_round } = round_durations in
  let open Stdlib in
  let round =
    if Compare.Int64.(delay_increment_per_round = Int64.zero) then
      (* Case when delay_increment_per_round is zero and a simple
         linear solution exists. *)
      Int64.div level_offset_in_seconds first_round_duration
    else
      (* Case when the increment is non-negative and we look for the
         quadratic solution. *)
      let pow_2 n = Int64.mul n n in
      let double n = Int64.shift_left n 1 in
      let times_8 n = Int64.shift_left n 3 in
      (* The integer square root is implemented using 4-adic
         induction. For any integer N, the convergence within the
         neighborhood of √N is ensured within log (N) steps. *)
      let rec sqrt (n : int64) =
        if Compare.Int64.(n = 0L) then 0L
        else
          let r = sqrt (Int64.shift_right n 2) in
          if Compare.Int64.(n < pow_2 (Int64.add (Int64.mul 2L r) 1L)) then
            Int64.mul 2L r
          else Int64.add (Int64.mul 2L r) 1L
      in

      let discr =
        Int64.add
          (pow_2
             (Int64.sub (double first_round_duration) delay_increment_per_round))
          (times_8
             (Int64.mul delay_increment_per_round level_offset_in_seconds))
      in
      Int64.div
        (Int64.add
           (Int64.sub delay_increment_per_round (double first_round_duration))
           (sqrt discr))
        (double delay_increment_per_round)
  in
  match level_offset_of_round round_durations ~round:(Int64.to_int32 round) with
  | Ok current_level_offset ->
      Ok
        {
          round = Int64.to_int32 round;
          offset = Int64.sub level_offset current_level_offset;
        }
  | Error _ as e -> e

(* typical mainnet values *)
let rd_duration =
  { first_round_duration = 30L; delay_increment_per_round = 15L }

let level_offsets ?(limit = 10000L) () =
  let l = ref [] in
  let v = ref 1L in
  while Int64.compare !v limit < 0 do
    l := !v :: !l;
    v := Stdlib.Int64.mul 4L !v
  done;
  !l

let run_test ~f level_offsets rd_duration =
  Stdlib.List.iter
    (fun loff ->
      ignore @@ f rd_duration ~level_offset:loff
      (* | Ok roff ->
       *    Format.printf "loff %Ld -> %a@." loff pp_roff roff
       * | Error  _  ->  Format.printf "loff %Ld -> err@." loff *))
    level_offsets

let test () =
  let level_offsets = level_offsets () in
  Core.Command.run
    (Bench.make_command
    @@ Stdlib.List.map
         (fun (name, f) ->
           Bench.Test.create ~name (fun () ->
               run_test level_offsets rd_duration ~f))
         [
           ("binsearch", round_and_offset);
           ("sqrt NR", sqrt_round_and_offset_v1);
           ("sqrt 4I", sqrt_round_and_offset_v2);
         ])

let pp_res ppf = function
  | Error _ -> Format.fprintf ppf "error"
  | Ok roff -> pp_roff ppf roff

let check () =
  Format.pp_open_vbox Format.std_formatter 0;
  Stdlib.List.iter (fun level_offset ->
      let r1 = round_and_offset rd_duration ~level_offset in
      let r11 = sqrt_round_and_offset_v1 rd_duration ~level_offset in
      let r2 = sqrt_round_and_offset_v2 rd_duration ~level_offset in
      Format.printf
        "@[<v 2>level_offset %Ld:@ - @[<h>witness: %a@]@ \
         - @[<h>sqrt NR: %a@]@ \
         - @[<h>sqrt 4I: %a@]@]@,"
        level_offset pp_res r1 pp_res r11 pp_res r2)
  @@ level_offsets ()
;;

check () ;;

test ()

