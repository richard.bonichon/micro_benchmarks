{ pkgs ? import <nixpkgs> { } }:
let
  camlStuff = with pkgs.ocaml-ng.ocamlPackages_latest; [
    ocaml
    core
    core_bench
    core_kernel
    core_extended
    dune_2
    merlin
    findlib
    zarith
    pkgs.ocamlformat
  ]; in
pkgs.stdenv.mkDerivation {
  name = "mlbenches";
  buildInputs = with pkgs; [
  ] ++ camlStuff;
}
